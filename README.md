# Saltstack Process Documentation

This repo contains Saltstack's standardize operating procedures, or "The Salty Way". This will include our
Development Framework, How-To documents, and other content.

# Sphinx

This has been moved to sphinx, leveraging gitlab pages. Please reference https://saltstack.gitlab.io/cicd/sops.
This page is only available to members of the Saltstack gitlab organization.

# How to contribute

The pipelines and automation has been setup to begin development right away. Start simply by forking this
repo, and adding to the docs directory. Be sure to include your changes in the index.rst file. After you have
pushed this to your fork, a pipeline will trigger which will end a pages deploy job. Once this job is complete
you may check your changes out on your repo's page, example:

https://saltstack.gitlab.io/employees/sre/felippeburk/sops
